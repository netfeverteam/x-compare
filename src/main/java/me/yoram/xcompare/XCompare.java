/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.xcompare;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * @author theyoz
 * @since 22/05/18
 */
public class XCompare {
    static Map<String, File> recurse(final File f, final int prefixLen) {
        final Map<String, File> res = new TreeMap<>();

        if (f.isFile()) {
            res.put(f.getAbsolutePath().substring(prefixLen), f);
        } else {
            final File[] files = f.listFiles();

            if (files != null) {
                for (final File o: files) {
                    if (o.isDirectory()) {
                        res.putAll(recurse(o, prefixLen));
                    } else {
                        res.put(o.getAbsolutePath().substring(prefixLen), o);
                    }
                }
            }
        }

        return res;
    }

    static void print(final String title, final Set<String> set) {
        System.out.println(title + ":");

        for (final String s: set) {
            System.out.println(s);
        }

        System.out.println();
    }

    public static void main(String[] args) throws Exception {
        File a = new File(args[0]);
        File b = new File(args[1]);

        Map<String, File> listA = recurse(a, a.getAbsolutePath().length());
        Map<String, File> listB = recurse(b, b.getAbsolutePath().length());

        // get unique in A
        final Set<String> uniqueA = new TreeSet<>();
        final Set<String> uniqueB = new TreeSet<>();
        final Set<String> differs = new TreeSet<>();

        for (final Map.Entry<String, File> entry: listA.entrySet()) {
            if (!listB.containsKey(entry.getKey())) {
                uniqueA.add(entry.getKey());
            } else {
                String md5A = DigestUtils.md5Hex(FileUtils.readFileToString(entry.getValue(), "UTF-8"));
                String md5B = DigestUtils.md5Hex(FileUtils.readFileToString(listB.get(entry.getKey()), "UTF-8"));

                if (!md5A.equals(md5B)) {
                    differs.add(entry.getKey());
                }
            }
        }

        for (final Map.Entry<String, File> entry: listB.entrySet()) {
            if (!listA.containsKey(entry.getKey())) {
                uniqueB.add(entry.getKey());
            }
        }

        print("UNIQUE IN A", uniqueA);
        print("UNIQUE IN B", uniqueB);
        print("DIFFERENCES", differs);
    }
}
